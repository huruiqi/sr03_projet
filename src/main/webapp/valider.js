function validateForm() {
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let password = document.getElementById("password").value;
    let email = document.getElementById("email").value;
    let isAdmin = document.getElementById("isAdmin").checked;

    let nameRegex = /^[A-Za-z]+$/; // 匹配由纯字母组成的字符串的正则表达式
    let passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/; // 匹配至少包含一个小写字母、一个大写字母、一个数字和一个特殊字符的密码的正则表达式

    if (!nameRegex.test(firstName)) {
        alert("First name should only contain letters");
        return false;
    }

    if (!nameRegex.test(lastName)) {
        alert("Last name should only contain letters");
        return false;
    }

    if (!passwordRegex.test(password)) {
        alert("Password should contain at least 1 uppercase, 1 lowercase, 1 digit, 1 special character and be at least 8 characters long");
        return false;
    }

    if (password.length < 8) {
        alert(`Password should be at least 8 characters long. You need ${8 - password.length} more character(s).`);
        return false;
    }

    if (!email.includes("@")) {
        alert("Invalid email address");
        return false;
    }

    return true;
}