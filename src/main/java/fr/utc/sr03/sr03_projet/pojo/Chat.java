package fr.utc.sr03.sr03_projet.pojo;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class Chat {
    private String id;
    private String titre;
    private String description;
    private DateTime horaire;
    private Integer duree;
    private User hote;
    private ArrayList<User> members;


    public Chat(String id, String titre, String description, DateTime horaire, Integer duree, User hote, ArrayList<User> members) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.horaire = horaire;
        this.duree = duree;
        this.hote = hote;
        this.members = members;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getHoraire() {
        return horaire;
    }

    public void setHoraire(DateTime horaire) {
        this.horaire = horaire;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public User getHote() {
        return hote;
    }

    public void setHote(User hote) {
        this.hote = hote;
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<User> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id='" + id + '\'' +
                ", titre='" + titre + '\'' +
                ", description='" + description + '\'' +
                ", horaire=" + horaire +
                ", duree=" + duree +
                ", hote=" + hote +
                ", members=" + members +
                '}';
    }
}
