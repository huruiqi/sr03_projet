package fr.utc.sr03.sr03_projet.pojo;

import fr.utc.sr03.sr03_projet.service.ChatService;

import java.util.ArrayList;

public class User {
    private String prenom;
    private String nom;
    private String email;
    private String password;
    private Boolean admin = false;
    private ArrayList<Chat> cree;
    private ArrayList<Chat> invite;

    public User() {
    }

    public User(String prenom, String nom, String email, String password, Boolean admin, ArrayList<Chat> cree, ArrayList<Chat> invite) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.admin = admin;
        this.cree = cree;
        this.invite = invite;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getAdmin() {
        return admin;
    }


    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public ArrayList<Chat> getCree() {
        return cree;
    }

    public void setCree(ArrayList<Chat> cree) {
        this.cree = cree;
    }

    public ArrayList<Chat> getInvite() {
        return invite;
    }

    public void setInvite(ArrayList<Chat> invite) {
        this.invite = invite;
    }

    @Override
    public String toString() {
        return "User{" +
                "prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", admin=" + admin +
                '}';
    }
}
