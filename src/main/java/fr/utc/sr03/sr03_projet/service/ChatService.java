package fr.utc.sr03.sr03_projet.service;

import fr.utc.sr03.sr03_projet.pojo.Chat;
import fr.utc.sr03.sr03_projet.pojo.User;

import java.util.ArrayList;

public interface ChatService {
    void addChat(Chat chat);
    Chat getChat(String cname);
    void delChat(String cname);

}
