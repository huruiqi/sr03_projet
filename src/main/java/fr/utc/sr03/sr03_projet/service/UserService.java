package fr.utc.sr03.sr03_projet.service;

import fr.utc.sr03.sr03_projet.pojo.Chat;
import fr.utc.sr03.sr03_projet.pojo.User;

import java.sql.Connection;
import java.util.ArrayList;

public interface UserService {
    void addUser(User user);

    User getUser(String uname);

    void delUser(String uname);

    ArrayList<Chat> getAllChatCree(User user);

    ArrayList<Chat> getAllChatInvite(User user);
}
